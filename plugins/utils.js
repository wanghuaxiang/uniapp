// #ifdef APP-PLUS
import { judgePermission } from './permission'
// #endif
import Vue from 'vue';
//金额过滤
Vue.filter('money', function(val) {
	console.log(val);
	if (val) {
		let value = Math.round(parseFloat(val) * 100) / 100;
		let valMoney = value.toString().split(".");
		if (valMoney.length == 1) {
			value = value.toString() + ".00";
			return value;
		}
		if (valMoney.length > 1) {
			if (valMoney[1].length < 2) {
				value = value.toString() + "0";
			}
			return value;
		}
		return value;
	} else {
		return "0.00";
	}
})
/**
 * 时间转换为XX前
 */
export const clickDateDiff = function (value) {
	var result;
	var minute = 1000 * 60;
	var hour = minute * 60;
	var day = hour * 24;
	var month = day * 30;
	var now = new Date().getTime();
	var diffValue = parseInt(now) - parseInt(value);
	if (diffValue < 0) {
		return;
	}
	var monthC = diffValue / month;
	var weekC = diffValue / (7 * day);
	var dayC = diffValue / day;
	var hourC = diffValue / hour;
	var minC = diffValue / minute;
	if (monthC >= 1) {
		result = "" + parseInt(monthC) + '月前';
	} else if (weekC >= 1) {
		result = "" + parseInt(weekC) + '周前';
	} else if (dayC >= 1) {
		result = "" + parseInt(dayC) + '天前';
	} else if (hourC >= 1) {
		result = "" + parseInt(hourC) + '小时前';
	} else if (minC >= 1) {
		result = "" + parseInt(minC) + '分钟前';
	} else {
		result = '刚刚';
	}
	return result;
};
/**
 * 时间戳转换为想要的时间格式
 */
//时间戳转换为时间 format('yyyy-MM-dd hh:mm:ss')
//时间格式转换
Date.prototype.format = function (fmt = 'yyyy-MM-dd hh:mm:ss') { //author: meizz 
	var o = {
		"M+": this.getMonth() + 1, //月份 
		"d+": this.getDate(), //日 
		"h+": this.getHours(), //小时 
		"m+": this.getMinutes(), //分 
		"s+": this.getSeconds(), //秒 
		"q+": Math.floor((this.getMonth() + 3) / 3), //季度 
		"S": this.getMilliseconds() //毫秒 
	};
	if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	for (var k in o)
		if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[
			k]).substr(("" + o[k]).length)));
	return fmt;
}
// 保存图片
let settingWritePhotosAlbum = false;
export const saveImg = function(url,callback) {
	if (url) {
		// #ifdef MP-WEIXIN
		if (settingWritePhotosAlbum) {
			uni.getSetting({
				success: res => {
					if (res.authSetting['scope.writePhotosAlbum']) {
                        uni.showLoading({
                            title: '正在下载'
                        });
						uni.downloadFile({
							url: url,
							success: data => {
								if (data.statusCode == 200) {
									uni.saveImageToPhotosAlbum({
										filePath: data.tempFilePath,
										success: () => {
                                            uni.hideLoading();
											callback && callback();
											uni.showToast({
												title: '保存成功'
											});
										},
                                        fail(e) {
                                            uni.hideLoading();
                                            tip({
                                                title: '下载失败，错误原因：' + e.errMsg,
                                                icon: "none"
                                            });
                                        }
									});
								} else {
                                    uni.hideLoading();
                                    uni.showToast({
                                    	title: '下载失败',
                                        icon: "none"
                                    });
                                }
							},
                            fail(e) {
                                uni.hideLoading();
                                uni.showToast({
                                	title: '下载失败，错误原因：' + e.errMsg,
                                    icon: "none"
                                });
                            }
						});
					} else {
						uni.showModal({
							title: '提示',
							content: '请先在设置页面打开“保存相册”使用权限',
							confirmText: '去设置',
							cancelText: '算了',
							success: data => {
								if (data.confirm) {
									uni.openSetting();
								}
							}
						});
					}
				}
			});
		} else {
			settingWritePhotosAlbum = true;
			uni.authorize({
				scope: 'scope.writePhotosAlbum',
				success: () => {
                    uni.showLoading({
                        title: '正在下载'
                    });
					uni.downloadFile({
						url: url,
						success: data => {
							if (data.statusCode == 200) {
								uni.saveImageToPhotosAlbum({
									filePath: data.tempFilePath,
									success: () => {
                                        uni.hideLoading();
										callback && callback();
										uni.showToast({
											title: '保存成功'
										});
									},
                                    fail(e) {
                                        uni.hideLoading();
                                        tip({
                                            title: '下载失败，错误原因：' + e.errMsg,
                                            icon: "none"
                                        });
                                    }
								});
							} else {
                                uni.hideLoading();
                                uni.showToast({
                                    title: '下载失败',
                                    icon: "none"
                                });
                            }
                        },
                        fail(e) {
                            uni.hideLoading();
                            uni.showToast({
                                title: '下载失败，错误原因：' + e.errMsg,
                                icon: "none"
                            });
                        }
					});
				}
			});
		}
		// #endif
		// #ifdef H5
        uni.showLoading({
            title: '正在下载'
        });
		uni.downloadFile({
			url: url,
			success: data => {
                uni.hideLoading();
				if (data.statusCode == 200) {
					callback && callback();
					window.open(data.tempFilePath);
				} else {
                    uni.showToast({
                        title: '下载失败',
                        icon: "none"
                    });
                }
            },
            fail(e) {
                uni.hideLoading();
                uni.showToast({
                    title: '下载失败，错误原因：' + e.errMsg,
                    icon: "none"
                });
            }
		});
		// #endif
		// #ifdef APP-PLUS
        uni.showLoading({
            title: '正在下载'
        });
		uni.saveImageToPhotosAlbum({
			filePath: url,
			success: () => {
                uni.hideLoading();
				callback && callback();
				uni.showToast({
					title: '保存成功'
				});
			},
            fail(e) {
                uni.hideLoading();
                uni.showToast({
                    title: '下载失败，错误原因：' + e.errMsg,
                    icon: "none"
                });
            }
		});
		// #endif
	} else {
		uni.showToast({
			title: '未找到图片',
			icon: 'none'
		});
	}
};
// 保存视频
function tip(data){
    setTimeout(() => {
        uni.showToast(data);
    },500);
}
export const saveVideo = function(url,callback) {
	if (url) {
		// #ifdef MP-WEIXIN
		if (settingWritePhotosAlbum) {
			uni.getSetting({
				success: res => {
					if (res.authSetting['scope.writePhotosAlbum']) {
                        // let urlArr = url.split("/");
                        // let updateUrl = urlArr[urlArr.length - 1];
                        // let  filePath = wx.env.USER_DATA_PATH + '/' + updateUrl;
                        uni.showLoading({
                            title: '正在下载'
                        });
						uni.downloadFile({
							url: url,
                            // filePath: filePath,
							success: data => {
								if (data.statusCode == 200) {
									uni.saveVideoToPhotosAlbum({
										filePath: data.tempFilePath,
										success: () => {
                                            uni.hideLoading();
											callback && callback();
											tip({
												title: '保存成功'
											});
										},
                                        fail(e) {
                                            uni.hideLoading();
                                            tip({
                                                title: '下载失败，错误原因：' + e.errMsg,
                                                icon: "none"
                                            });
                                        }
									});
								} else {
                                    uni.hideLoading();
                                    tip({
                                        title: '下载失败',
                                        icon: "none"
                                    });
                                }
                            },
                            fail(e) {
                                uni.hideLoading();
                                tip({
                                    title: '下载失败，错误原因：' + e.errMsg,
                                    icon: "none"
                                });
                            }
						});
					} else {
						uni.showModal({
							title: '提示',
							content: '请先在设置页面打开“保存相册”使用权限',
							confirmText: '去设置',
							cancelText: '算了',
							success: data => {
								if (data.confirm) {
									uni.openSetting();
								}
							}
						});
					}
				}
			});
		} else {
			settingWritePhotosAlbum = true;
			uni.authorize({
				scope: 'scope.writePhotosAlbum',
				success: () => {
                    // let urlArr = url.split("/");
                    // let updateUrl = urlArr[urlArr.length - 1];
                    // let filePath = wx.env.USER_DATA_PATH + '/' + updateUrl;
                    uni.showLoading({
                        title: '正在下载'
                    });
					uni.downloadFile({
						url: url,
                        // filePath: filePath,
						success: data => {
							if (data.statusCode == 200) {
								uni.saveVideoToPhotosAlbum({
									filePath: data.tempFilePath,
									success: () => {
                                        uni.hideLoading();
										callback && callback();
										tip({
											title: '保存成功'
										});
									},
                                    fail(e) {
                                        console.log("-----------------2", e);
                                        uni.hideLoading();
                                        tip({
                                            title: '下载失败，错误原因：'+ e.errMsg,
                                            icon: "none"
                                        });
                                    }
								});
							} else {
                                uni.hideLoading();
                                tip({
                                    title: '下载失败，错误原因：'+ data.errMsg,
                                    icon: "none"
                                });
                            }
                        },
                        fail(e) {
                            console.log("-----------------", e);
                            uni.hideLoading();
                            tip({
                                title: '下载失败，错误原因：' + e.errMsg,
                                icon: "none"
                            });
                        }
					});
				}
			});
		}
		// #endif
		// #ifdef H5
        uni.showLoading({
            title: '正在下载'
        });
		uni.downloadFile({
			url: url,
			success: data => {
                uni.hideLoading();
				if (data.statusCode == 200) {
					callback && callback();
					window.open(data.tempFilePath);
				} else {
                    tip({
                        title: '下载失败',
                        icon: "none"
                    });
                }
            },
            fail(e) {
                uni.hideLoading();
                tip({
                    title: '下载失败，错误原因：' + e.errMsg,
                    icon: "none"
                });
            }
		});
		// #endif
		// #ifdef APP-PLUS
        uni.showLoading({
            title: '正在下载'
        });
		uni.saveVideoToPhotosAlbum({
			filePath: url,
			success: () => {
                uni.hideLoading();
				callback && callback();
				tip({
					title: '保存成功'
				});
			},
            fail(e) {
                uni.hideLoading();
                tip({
                    title: '下载失败，错误原因：' + e.errMsg,
                    icon: "none"
                });
            }
		});
		// #endif
	} else {
		tip({
			title: '未找到视频',
			icon: 'none'
		});
	}
};
// 微信小程序获取定位权限判断
function wxAppletsLocation(successCallback, errCallback) {
	uni.getSetting({
		success: res => {
			if (res.authSetting['scope.userLocation']) {
				uni.getLocation({
					type: 'gcj02',
					success: res => {
						successCallback(res);
					},
					fail: (err) => {
						console.log("位置信息错误", err);
						errCallback("位置信息获取失败");
					}
				});
			} else {
				errCallback("“位置信息”未授权");
				uni.showModal({
					title: '提示',
					content: '请先在设置页面打开“位置信息”使用权限',
					confirmText: '去设置',
					cancelText: '再逛会',
					success: res => {
						if (res.confirm) {
							uni.openSetting();
						}
					}
				});
			}
		}
	});
}
// 获取地址信息
let locationAuthorize = true;
export const getAppWxLatLon = function(successCallback, errCallback) {
	const _this = this;
	// #ifdef MP
	if (locationAuthorize) {
		uni.authorize({
			scope: 'scope.userLocation',
			success: () => {
				wxAppletsLocation(successCallback, errCallback);
				locationAuthorize = false;
			},
			fail: () => {
				locationAuthorize = false;
			}
		});
	} else {
		wxAppletsLocation(successCallback, errCallback);
	}
	// #endif
	// #ifdef APP-PLUS
	judgePermission("location", function(result) {
		if (result == 1) {
			uni.getLocation({
				type: 'gcj02',
				success: res => {
					// store.commit("setCurrentAddress", {
					// 	latitude: res.latitude,
					// 	longitude: res.longitude
					// });
					successCallback(res);
				},
				fail: (err) => {
					console.log("位置信息错误", err);
					errCallback("位置信息获取失败");
				}
			});
		}
	});
	// #endif
}

// 判断类型集合
export const checkStr = (str, type) => {
	switch (type) {
		case 'mobile': //手机号码
			return /^1[3|4|5|6|7|8|9][0-9]{9}$/.test(str);
		case 'tel': //座机
			return /^(0\d{2,3}-\d{7,8})(-\d{1,4})?$/.test(str);
		case 'card': //身份证
			return /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(str);
		case 'mobileCode': //6位数字验证码
			return /^[0-9]{6}$/.test(str)
		case 'pwd': //密码以字母开头，长度在6~18之间，只能包含字母、数字和下划线
			return /^([a-zA-Z0-9_]){6,18}$/.test(str)
		case 'payPwd': //支付密码 6位纯数字
			return /^[0-9]{6}$/.test(str)
		case 'postal': //邮政编码
			return /[1-9]\d{5}(?!\d)/.test(str);
		case 'QQ': //QQ号
			return /^[1-9][0-9]{4,9}$/.test(str);
		case 'email': //邮箱
			return /^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/.test(str);
		case 'money': //金额(小数点2位)
			return /^\d*(?:\.\d{0,2})?$/.test(str);
		case 'URL': //网址
			return /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?/.test(str)
		case 'IP': //IP
			return /((?:(?:25[0-5]|2[0-4]\\d|[01]?\\d?\\d)\\.){3}(?:25[0-5]|2[0-4]\\d|[01]?\\d?\\d))/.test(str);
		case 'date': //日期时间
			return /^(\d{4})\-(\d{2})\-(\d{2}) (\d{2})(?:\:\d{2}|:(\d{2}):(\d{2}))$/.test(str) || /^(\d{4})\-(\d{2})\-(\d{2})$/
				.test(str)
		case 'number': //数字
			return /^[0-9]$/.test(str);
		case 'english': //英文
			return /^[a-zA-Z]+$/.test(str);
		case 'chinese': //中文
			return /^[\\u4E00-\\u9FA5]+$/.test(str);
		case 'lower': //小写
			return /^[a-z]+$/.test(str);
		case 'upper': //大写
			return /^[A-Z]+$/.test(str);
		case 'HTML': //HTML标记
			return /<("[^"]*"|'[^']*'|[^'">])*>/.test(str);
		default:
			return true;
	}
}